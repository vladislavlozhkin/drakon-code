import * as Database from "../database";

async function makeBody(body) {
  const db = await Database.get();

  let result = [];

  for (let item of body) {
    let doc = null;
    let data = null;

    switch (item.type) {
      case "ACTION":
        doc = await db.actions
          .findOne()
          .where("_id")
          .eq(item.id)
          .exec();

        item.data = doc._data;
        break;
      case "FORK":
        doc = await db.forks
          .findOne()
          .where("_id")
          .eq(item.id)
          .exec();

        const yes = await makeBody(doc._data.yes);
        const no = await makeBody(doc._data.no);

        item.question = doc._data.question;
        item.yes = yes;
        item.no = no;
        break;
      case "FORLOOP":
        doc = await db.forloops
          .findOne()
          .where("_id")
          .eq(item.id)
          .exec();

        let data = {};
        data.code = doc._data.code;
        data.body = await makeBody(doc._data.body);

        item.data = data;
        break;
      case "SWITCH":
        doc = await db.switches
          .findOne()
          .where("_id")
          .eq(item.id)
          .exec();

        const cases = [...doc.cases];

        for (let caseItem of cases) {
          caseItem.body = await makeBody(caseItem.body);
        }

        item.data = {
          choice: doc.choice,
          cases
        };

      default:
        break;
    }

    result.push(item);
  }

  return result;
}

export default makeBody;

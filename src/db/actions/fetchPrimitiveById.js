import * as Database from "../database";

import makeBody from "./makeBody";

async function fetchPrimitiveById(id) {
  const db = await Database.get();

  const doc = await db.primitives
    .findOne()
    .where("_id")
    .eq(id)
    .exec();

  const primitive = doc._data;

  const body = await makeBody(primitive.body);

  return primitive;
}

export default fetchPrimitiveById;

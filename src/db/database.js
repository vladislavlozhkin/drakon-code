import * as RxDB from "rxdb";
import { QueryChangeDetector } from "rxdb";

QueryChangeDetector.enable();
QueryChangeDetector.enableDebugging();

RxDB.plugin(require("pouchdb-adapter-idb"));

const collections = [
  {
    name: "actions",
    schema: require("./schemes/Action").default
  },
  {
    name: "forks",
    schema: require("./schemes/Fork").default
  },
  {
    name: "forloops",
    schema: require("./schemes/ForLoop").default
  },
  {
    name: "primitives",
    schema: require("./schemes/Primitive").default
  },
  {
    name: "switches",
    schema: require("./schemes/Switch").default
  },
  {
    name: "tabs",
    schema: require("./schemes/Tabs").default
  }
];

let dbPromise = null;

const _create = async function() {
  console.log("DatabaseService: creating database..");
  const db = await RxDB.create({
    name: "drakondb",
    adapter: "idb",
    password: "myLongAndStupidPassword"
  });
  console.log("DatabaseService: created database");

  window["db"] = db; // write to window for debugging

  // show leadership in title
  db.waitForLeadership().then(() => {
    console.log("isLeader now");
    document.title = "♛ " + document.title;
  });

  // create collections
  console.log("DatabaseService: create collections");
  await Promise.all(collections.map(colData => db.collection(colData)));

  // sync
  return db;
};

export function get() {
  if (!dbPromise) dbPromise = _create();
  return dbPromise;
}

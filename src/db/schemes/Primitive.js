export default {
  title: "primitive schema",
  version: 0,
  description: "describes a primitive",
  type: "object",
  properties: {
    title: {
      type: "object",
      properties: {
        code: "string"
      }
    },
    params: {
      type: "array",
      items: {
        type: "object",
        properties: {
          name: { type: "string" },
          value: { type: "string" }
        }
      }
    },
    body: {
      type: "array",
      items: {
        type: "object",
        properties: {
          type: { type: "string" },
          id: { type: "string" }
        }
      }
    }
  }
};

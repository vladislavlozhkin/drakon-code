export default {
  title: "switch schema",
  version: 0,
  description: "describes a switch",
  type: "object",
  properties: {
    choice: {
      type: "object",
      properties: {
        code: { type: "string" }
      }
    },
    cases: {
      type: "array",
      items: {
        type: "object",
        properties: {
          code: { type: "string" },
          body: {
            type: "array",
            items: {
              type: "object",
              properties: {
                type: { type: "string" },
                id: { type: "string" }
              }
            }
          }
        }
      }
    }
  }
};

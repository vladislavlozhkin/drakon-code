export default {
  title: "action schema",
  version: 0,
  description: "describes a action",
  type: "object",
  properties: {
    code: { type: "string" }
  }
};

export default {
  title: "fork schema",
  version: 0,
  description: "describes a fork",
  type: "object",
  properties: {
    question: {
      type: "object",
      properties: {
        code: { type: "string" }
      }
    },
    yes: {
      type: "array",
      items: {
        type: "object",
        properties: {
          type: { type: "string" },
          id: { type: "string" }
        }
      }
    },
    no: {
      type: "array",
      items: {
        type: "object",
        properties: {
          type: { type: "string" },
          id: { type: "string" }
        }
      }
    }
  }
};

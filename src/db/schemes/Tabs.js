export default {
  title: "Workspace schema",
  version: 0,
  description: "describes a action",
  type: "object",
  properties: {
    type: { type: "string" },
    id: { type: "string" }
  }
};

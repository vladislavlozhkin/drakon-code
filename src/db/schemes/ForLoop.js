export default {
  title: "forloop schema",
  version: 0,
  description: "describes a forloop",
  type: "object",
  properties: {
    code: { type: "string" },
    body: {
      type: "array",
      items: {
        type: "object",
        properties: {
          type: { type: "string" },
          id: { type: "string" }
        }
      }
    }
  }
};

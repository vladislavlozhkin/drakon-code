function uid(size = 2) {
  let result = "uid";

  for (let i = 0; i <= size; i++) {
    result += s4();
  }

  return result;
}

function s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
}

export default uid;

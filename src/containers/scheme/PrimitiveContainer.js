import React, { Component } from "react";
import PropTypes from "prop-types";

import fetchPrimitiveById from "../../db/actions/fetchPrimitiveById";
import Primitive from "../../components/scheme/Primitive";

class PrimitiveContainer extends Component {
  state = {
    ast: null
  };

  componentWillMount() {
    this.fetchAst();
  }

  async fetchAst() {
    const primitive = await fetchPrimitiveById(this.props.id);

    this.setState({ ast: primitive });
  }

  render() {
    const { ast } = this.state;

    return <Primitive ast={ast} />;
  }
}

PrimitiveContainer.propTypes = {
  id: PropTypes.string.isRequired
};

export default PrimitiveContainer;

import React, { Component } from "react";
import App from "../components/App";

import * as Database from "../db/database";

class AppContainer extends Component {
  state = {
    activeTab: null
  };

  onSelectTab = tab => {
    this.setState({ activeTab: tab });
  };

  render() {
    const { activeTab } = this.state;

    return <App onSelectTab={this.onSelectTab} activeTab={activeTab} />;
  }
}

export default AppContainer;

import React, { Component } from "react";
import PropTypes from "prop-types";

import * as Database from "../db/database";

import Sidebar from "../components/Sidebar";

class SidebarContainer extends Component {
  state = {
    primitives: null
  };

  constructor(props) {
    super(props);

    this.subs = [];
  }

  async componentWillMount() {
    this.db = await Database.get();
    this.subscribePrimitives();
  }

  subscribePrimitives() {
    const sub = this.db.primitives.find().$.subscribe(primitives => {
      this.setState({ primitives });
    });

    this.subs.push(sub);
  }

  componentWillUnmount() {
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
  }

  pushTab = async tab => {
    const oldTab = await this.db.tabs
      .findOne()
      .where("id")
      .eq(tab.id)
      .exec();

    if (!oldTab) {
      this.db.tabs.insert(tab);
    }
  };

  createPrimitive = name => {
    this.db.primitives.insert({ title: { code: name }, params: [], body: [] });
  };

  render() {
    const { primitives } = this.state;
    return (
      <Sidebar
        primitives={primitives}
        pushTab={this.pushTab}
        createPrimitive={this.createPrimitive}
      />
    );
  }
}

export default SidebarContainer;

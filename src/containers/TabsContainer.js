import React, { Component } from "react";
import PropTypes from "prop-types";

import Tabs from "../components/Tabs";

import * as Database from "../db/database";

class TabsContainer extends Component {
  state = {
    tabs: null,
    activeTab: null
  };

  constructor(props) {
    super(props);

    this.subs = [];
    this.db = null;
  }

  async componentWillMount() {
    const db = await Database.get();
    this.db = db;
    this.subscribeTabs(db);
  }

  componentDidUpdate() {
    const { tabs } = this.state;
    if (tabs) {
      const tab = tabs[0];

      if (tab && !this.state.activeTab) {
        this.selectTab(tab._data);
      }
    }
  }

  componentWillUnmount() {
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
  }

  selectTab = tab => {
    this.setState({ activeTab: tab });
    this.props.onSelectTab(tab);
  };

  closeTab = async tab => {
    const doc = await this.db.tabs
      .findOne()
      .where("_id")
      .eq(tab._id)
      .exec();

    if (doc) {
      doc.remove();
    }
  };

  subscribeTabs(db) {
    const sub = db.tabs.find().$.subscribe(tabs => {
      this.setState({ tabs });
    });

    this.subs.push(sub);
  }

  render() {
    const { tabs, activeTab } = this.state;
    return (
      <Tabs
        tabs={tabs}
        activeTab={activeTab}
        onSelectTab={this.selectTab}
        onCloseTab={this.closeTab}
      />
    );
  }
}

Tabs.propTypes = {
  onSelectTab: PropTypes.func
};

export default TabsContainer;

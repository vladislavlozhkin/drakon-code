import React from "react";
import PropTypes from "prop-types";

import PrimitiveContainer from "../containers/scheme/PrimitiveContainer";
import SidebarContainer from "../containers/SidebarContainer";
import TabsContainer from "../containers/TabsContainer";
import "../styles/App.css";

function App(props) {
  const { onSelectTab, activeTab } = props;

  let primitiveId = null;

  if (activeTab) {
    primitiveId = activeTab.id;
  }

  return (
    <div className="App">
      <div className="App__sidebar">
        <SidebarContainer />
      </div>
      <div className="App__container">
        <TabsContainer onSelectTab={onSelectTab} />
        <div className="App__scheme-container">
          <svg className="App__scheme">
            {primitiveId && <PrimitiveContainer id={primitiveId} />}
          </svg>
        </div>
      </div>
    </div>
  );
}

App.propTypes = {
  activeTab: PropTypes.object,
  onSelectTab: PropTypes.func
};

export default App;

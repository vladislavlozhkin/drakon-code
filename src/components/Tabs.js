import React from "react";
import PropTypes from "prop-types";

import Tab from "./Tab";

import "../styles/Tabs.css";

function Tabs(props) {
  const { tabs, activeTab, onSelectTab, onCloseTab } = props;
  return (
    <div className="Tabs">
      {tabs &&
        activeTab &&
        tabs.map((tab, index) => (
          <Tab
            data={tab}
            isActive={activeTab.id == tab.id}
            onSelectTab={onSelectTab}
            onCloseTab={onCloseTab}
          />
        ))}
    </div>
  );
}

Tabs.propTypes = {
  tabs: PropTypes.arrayOf(PropTypes.object).isRequired,
  activeTab: PropTypes.object,
  onSelectTab: PropTypes.func,
  onCloseTab: PropTypes.func
};

export default Tabs;

import React, { Component } from "react";
import PropTypes from "prop-types";
import Yoga from "yoga-layout";

class YogaNode extends Component {
  state = {
    node: null,
    layout: {}
  };

  componentWillMount() {
    this.makeNode();
  }

  componentDidMount() {
    this.updateLayout();
  }

  componentDidUpdate() {
    this.setStyles(this.state.node);
    this.updateLayout();
  }

  getChildContext() {
    return { parent: this.state.node };
  }

  updateLayout() {
    const stateLayout = this.state.layout;
    this.context.root.calculateLayout();
    const layout = this.state.node.getComputedLayout();

    if (stateLayout.toString() !== layout.toString()) {
      this.setState({ layout });
    }

    if (this.props.onUpdateLayout) {
      this.props.onUpdateLayout(layout);
    }
  }

  makeNode() {
    const { parent, root } = this.context;
    const node = Yoga.Node.create();

    if (parent) {
      parent.insertChild(node, parent.getChildCount());
      root.calculateLayout();
    }

    this.setStyles(node);
    this.setState({ node });
    this.setState({ layout: node.getComputedLayout() });
  }

  setStyles(node) {
    if (!this.props.styles) return false;

    const { styles } = this.props;
    const { root } = this.context;

    for (let methodName in styles) {
      const params = styles[methodName];
      const param1 = params[0];
      const param2 = params[1];
      if (param2) {
        node[methodName](param1, param2);
      } else {
        node[methodName](param1);
      }
    }

    root.calculateLayout();
  }

  render() {
    const { left, top } = this.state.layout;
    const styles = {
      transform: `translate(${left}px, ${top}px)`
    };

    return <g style={styles}>{this.props.children}</g>;
  }
}

YogaNode.propTypes = {
  styles: PropTypes.object,
  onUpdateLayout: PropTypes.func
};

YogaNode.contextTypes = {
  parent: PropTypes.object,
  root: PropTypes.object
};

YogaNode.childContextTypes = {
  parent: PropTypes.object
};

export default YogaNode;

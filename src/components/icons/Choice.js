import React from "react";
import PropTypes from "prop-types";
import BaseIcon from "./BaseIcon";
import Highlight from "../Highlight.js";

import "../../styles/Choice.css";

class Choice extends BaseIcon {
  state = {
    bgPoints: []
  };

  componentDidUpdate() {
    this.updatePolygonPoints();
  }

  updatePolygonPoints() {
    const code = this.fObject.querySelector("code");
    const width = code.offsetWidth;
    const height = code.offsetHeight;

    const points = [
      0,
      0,
      width,
      0,
      width - 15,
      height,
      0,
      height,
      0,
      height / 2
    ];

    if (this.state.bgPoints.toString() !== points.toString()) {
      this.setState({ bgPoints: points });
    }
  }

  render() {
    const { code } = this.props;

    return (
      <React.Fragment>
        <polygon className="Choice-bg" points={this.state.bgPoints} />
        <foreignObject
          className="Choice"
          ref={ref => {
            this.fObject = ref;
          }}
        >
          <Highlight code={code} />
        </foreignObject>
      </React.Fragment>
    );
  }
}

Choice.propTypes = {
  code: PropTypes.string.isRequired
};

export default Choice;

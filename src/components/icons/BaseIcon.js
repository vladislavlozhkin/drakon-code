import { Component } from "react";
import PropTypes from "prop-types";

class BaseIcon extends Component {
  state = {
    width: 0,
    height: 0
  };

  componentDidMount() {
    this.getIconSize();
  }

  getIconSize() {
    const code = this.fObject.querySelector("code");
    const width = code.offsetWidth;
    const height = code.offsetHeight;

    this.setState({ width, height });
    this.resizeParen(width, height);
  }

  resizeParen(width, height) {
    const { parent, root } = this.context;
    parent.setWidth(width);
    parent.setHeight(height);

    root.calculateLayout();
  }
}

BaseIcon.contextTypes = {
  parent: PropTypes.object,
  root: PropTypes.object
};

export default BaseIcon;

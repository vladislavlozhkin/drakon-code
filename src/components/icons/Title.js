import React from "react";
import PropTypes from "prop-types";

import BaseIcon from "./BaseIcon";

import "../../styles/Title.css";

class Title extends BaseIcon {
  render() {
    const { code } = this.props;

    return (
      <foreignObject
        className="Title"
        ref={ref => {
          this.fObject = ref;
        }}
      >
        <pre>
          <code className="js hljs javascript">{code}</code>
        </pre>
      </foreignObject>
    );
  }
}

Title.propTypes = {
  code: PropTypes.string.isRequired
};

export default Title;

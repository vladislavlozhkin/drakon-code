import React from "react";
import PropTypes from "prop-types";

import BaseIcon from "./BaseIcon";

import "../../styles/End.css";

class End extends BaseIcon {
  render() {
    const { code } = this.props;

    return (
      <foreignObject
        className="End"
        ref={ref => {
          this.fObject = ref;
        }}
      >
        <pre>
          <code className="js hljs javascript">End</code>
        </pre>
      </foreignObject>
    );
  }
}

End.propTypes = {
  code: PropTypes.string.isRequired
};

export default End;

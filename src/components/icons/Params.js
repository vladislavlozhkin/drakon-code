import React from "react";
import PropTypes from "prop-types";
import BaseIcon from "./BaseIcon";
import Highlight from "../Highlight.js";

import "../../styles/Params.css";

class Params extends BaseIcon {
  render() {
    const { params } = this.props;
    const paramsString = params
      .map(param => {
        let result = param.name;
        if (param.value) {
          result += ` = ${param.value}`;
        }
        return result;
      })
      .join("\n");

    return (
      <foreignObject
        className="Params"
        ref={ref => {
          this.fObject = ref;
        }}
      >
        <Highlight code={paramsString} />
      </foreignObject>
    );
  }
}

Params.propTypes = {
  params: PropTypes.array.isRequired
};

export default Params;

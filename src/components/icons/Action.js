import React from "react";
import PropTypes from "prop-types";
import BaseIcon from "./BaseIcon";
import Highlight from "../Highlight.js";

import "../../styles/Action.css";

class Action extends BaseIcon {
  render() {
    const { data } = this.props;

    return (
      <foreignObject
        className="Action"
        ref={ref => {
          this.fObject = ref;
        }}
      >
        <Highlight code={data.code} />
      </foreignObject>
    );
  }
}

Action.propTypes = {
  data: PropTypes.object.isRequired
};

export default Action;

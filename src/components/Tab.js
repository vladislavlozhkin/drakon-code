import React from "react";
import PropTypes from "prop-types";

import "../styles/Tab.css";

function Tab(props) {
  const { data, isActive, onSelectTab, onCloseTab } = props;

  return (
    <div
      className={`Tab ${isActive ? "Tab_state_active" : null}`}
      onClick={() => onSelectTab(data._data)}
      key={"tab"}
    >
      {data.id}
      <button
        className="Tab__close-icon"
        onClick={() => onCloseTab(data._data)}
      >
        ×
      </button>
    </div>
  );
}

Tab.propTypes = {
  data: PropTypes.object.isRequired,
  isActive: PropTypes.bool.isRequired,
  onSelectTab: PropTypes.func.isRequired,
  onCloseTab: PropTypes.func.isRequired
};

export default Tab;

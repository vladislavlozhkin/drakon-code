import React from "react";
import PropTypes from "prop-types";
import FaPlus from "react-icons/lib/fa/plus";

import "../styles/Sidebar.css";

function onSubimtForm(e, handler) {
  const name = e.target.name.value;
  handler(name);
  e.target.reset();
  e.preventDefault();
}

function Sidebar(props) {
  const { primitives, pushTab, createPrimitive } = props;

  return (
    <div className="Sidebar">
      <h3 className="Sidebar__title">Примитивы</h3>
      <form
        className="Sidebar__add-form"
        onSubmit={e => onSubimtForm(e, createPrimitive)}
      >
        <input type="text" name="name" />
        <button type="submit">
          <FaPlus size={20} />
        </button>
      </form>
      <ul className="Sidebar__list">
        {primitives &&
          primitives.map(primitive => (
            <li
              className="Sidebar__list-item"
              key={primitive._id}
              onClick={() => pushTab({ type: "PRIMITIVE", id: primitive._id })}
            >
              {primitive.title.code}
            </li>
          ))}
      </ul>
    </div>
  );
}

Sidebar.propTypes = {
  primitives: PropTypes.arrayOf(PropTypes.object).isRequired,
  pushTab: PropTypes.func.isRequired,
  createPrimitive: PropTypes.func.isRequired
};

export default Sidebar;

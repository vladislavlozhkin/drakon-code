import React from "react";
import Highlightjs from "react-highlight.js";
import "../../node_modules/highlight.js/styles/atom-one-dark.css";
import "../styles/hljs.css";

function Highlight(props) {
  const { code } = props;
  return <Highlightjs language="js">{code}</Highlightjs>;
}

export default Highlight;

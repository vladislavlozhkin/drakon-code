import React, { Component } from "react";
import PropTypes from "prop-types";
import Yoga from "yoga-layout";

import ForLoopOpen from "../icons/ForLoopOpen";
import YogaNode from "../YogaNode";
import Body from "./Body";

class ForLoop extends Component {
  state = {
    baseLinePoints: [],
    closeLinePoints: [],
    containerLayout: {},
    forLoopOpenLayout: {}
  };

  componentDidUpdate() {
    this.setLinePoints();
    this.setCloseLinesPoints();
  }

  setLinePoints() {
    const { containerLayout, forLoopOpenLayout } = this.state;
    const top = forLoopOpenLayout.height;
    const bottom = containerLayout.height - 5;
    const x = -7;
    const baseLinePoints = [x, top, x, bottom];

    const current = this.state.baseLinePoints.toString();
    const next = baseLinePoints.toString();
    if (current !== next) {
      this.setState({ baseLinePoints });
    }
  }

  setCloseLinesPoints() {
    const { containerLayout, forLoopOpenLayout } = this.state;
    const closeLinePoints = [
      [-7, containerLayout.height - 5],
      [0, containerLayout.height],
      [forLoopOpenLayout.width - 7, containerLayout.height],
      [forLoopOpenLayout.width, containerLayout.height - 5]
    ];

    const current = this.state.closeLinePoints.toString();
    const next = closeLinePoints.toString();
    if (current !== next) {
      this.setState({ closeLinePoints });
    }
  }

  setContainerLayout = layout => {
    const current = this.state.containerLayout.toString();
    const next = layout.toString();
    if (current !== next) {
      this.setState({ containerLayout: layout });
    }
  };

  setForLoopOpenLayout = layout => {
    const current = this.state.forLoopOpenLayout.toString();
    const next = layout.toString();
    if (current !== next) {
      this.setState({ forLoopOpenLayout: layout });
    }
  };

  render() {
    const { baseLinePoints, closeLinePoints } = this.state;
    const { data } = this.props.ast;
    const styles = {
      forLoopOpen: {
        setMargin: [Yoga.EDGE_BOTTOM, 10]
      }
    };

    return (
      <React.Fragment>
        <polyline
          fill="none"
          stroke="#282c34"
          strokeWidth={2}
          points={baseLinePoints}
        />
        <YogaNode onUpdateLayout={this.setContainerLayout}>
          <YogaNode
            styles={styles.forLoopOpen}
            onUpdateLayout={this.setForLoopOpenLayout}
          >
            <ForLoopOpen code={data.code} />
          </YogaNode>
          <Body ast={data.body} />
          <polyline
            fill="none"
            stroke="#282c34"
            strokeWidth={4}
            points={closeLinePoints}
          />
        </YogaNode>
      </React.Fragment>
    );
  }
}

ForLoop.propTypes = {
  ast: PropTypes.object.isRequired
};

export default ForLoop;

import React, { Component } from "react";
import PropTypes from "prop-types";
import Yoga from "yoga-layout";

import YogaNode from "../YogaNode";
import Action from "../icons/Action";
import Fork from "./Fork";
import Switch from "./Switch";
import ForLoop from "./ForLoop";

class Body extends Component {
  switchItemByType(node) {
    let result = null;

    switch (node.type) {
      case "ACTION":
        result = <Action data={node.data} />;
        break;
      case "FORK":
        result = <Fork ast={node} />;
        break;
      case "SWITCH":
        result = <Switch ast={node} />;
        break;
      case "FORLOOP":
        result = <ForLoop ast={node} />;
        break;
      default:
        result = null;
    }

    return result;
  }

  render() {
    const { ast } = this.props;

    const bodyRowStyle = {
      setMargin: [Yoga.EDGE_BOTTOM, 10]
    };

    return (
      <React.Fragment>
        {ast.map((node, i) => (
          <YogaNode styles={bodyRowStyle} key={"body-row-index" + i}>
            {this.switchItemByType(node)}
          </YogaNode>
        ))}
      </React.Fragment>
    );
  }
}

Body.propTypes = {
  ast: PropTypes.array.isRequired
};

export default Body;

import React, { Component } from "react";
import PropTypes from "prop-types";
import Yoga from "yoga-layout";

import Question from "../icons/Question";
import YogaNode from "../YogaNode";
import Body from "./Body";

class Fork extends Component {
  state = {
    wrapperLayout: {},
    leftBranchLayout: {},
    rightBranchLayout: {},
    questionLayout: {},
    topLinePoints: [],
    rightLinePoints: [],
    bottomLinePoints: []
  };

  componentDidUpdate() {
    this.setLinesPoints();
  }

  setLinesPoints() {
    this.setTopLinePoints();
    this.setRigthLinePoints();
    this.setBottomLinePoints();
  }

  setTopLinePoints() {
    const { rightBranchLayout, questionLayout } = this.state;
    const top = questionLayout.height / 2;
    const right = rightBranchLayout.left;
    const topLinePoints = [0, top, right, top];

    const current = this.state.topLinePoints.toString();
    const next = topLinePoints.toString();
    if (current !== next) {
      this.setState({ topLinePoints });
    }
  }

  setRigthLinePoints() {
    const { wrapperLayout, rightBranchLayout, questionLayout } = this.state;
    const top = questionLayout.height / 2;
    const right = rightBranchLayout.left;
    const bottom = wrapperLayout.height;
    const rightLinePoints = [right, top, right, bottom];

    const current = this.state.rightLinePoints.toString();
    const next = rightLinePoints.toString();
    if (current !== next) {
      this.setState({ rightLinePoints });
    }
  }

  setBottomLinePoints() {
    const { wrapperLayout, rightBranchLayout } = this.state;
    const right = rightBranchLayout.left;
    const bottom = wrapperLayout.height;
    const bottomLinePoints = [right, bottom, 0, bottom];

    const current = this.state.bottomLinePoints.toString();
    const next = bottomLinePoints.toString();
    if (current !== next) {
      this.setState({ bottomLinePoints });
    }
  }

  setWrapperLayout = layout => {
    const current = this.state.wrapperLayout.toString();
    const next = layout.toString();
    if (current !== next) {
      this.setState({ wrapperLayout: layout });
    }
  };

  setLeftBranchLayout = layout => {
    const current = this.state.leftBranchLayout.toString();
    const next = layout.toString();
    if (current !== next) {
      this.setState({ leftBranchLayout: layout });
    }
  };

  setRightBranchLayout = layout => {
    const current = this.state.rightBranchLayout.toString();
    const next = layout.toString();
    if (current !== next) {
      this.setState({ rightBranchLayout: layout });
    }
  };

  setQuestionLayout = layout => {
    const current = this.state.questionLayout.toString();
    const next = layout.toString();
    if (current !== next) {
      this.setState({ questionLayout: layout });
    }
  };

  render() {
    const { question, yes, no } = this.props.ast;

    const wrapperStyles = {
      setFlexDirection: [Yoga.FLEX_DIRECTION_ROW]
    };
    const leftBranchStyles = {
      setMargin: [Yoga.EDGE_RIGHT, 20]
    };
    const rightBranchStyles = {
      setMargin: [Yoga.EDGE_TOP, 37]
    };
    const questionWrapperStyles = {
      setMargin: [Yoga.EDGE_BOTTOM, 10]
    };

    return (
      <React.Fragment>
        <polyline
          fill="none"
          stroke="#282c34"
          strokeWidth={2}
          points={this.state.topLinePoints}
        />
        <polyline
          fill="none"
          stroke="#282c34"
          strokeWidth={2}
          points={this.state.rightLinePoints}
        />
        <polyline
          fill="none"
          stroke="#282c34"
          strokeWidth={2}
          points={this.state.bottomLinePoints}
        />
        <YogaNode styles={wrapperStyles} onUpdateLayout={this.setWrapperLayout}>
          <YogaNode
            styles={leftBranchStyles}
            onUpdateLayout={this.setLeftBranchLayout}
          >
            <YogaNode
              styles={questionWrapperStyles}
              onUpdateLayout={this.setQuestionLayout}
            >
              <Question code={question.code} />
            </YogaNode>
            <Body ast={no} />
          </YogaNode>
          <YogaNode
            styles={rightBranchStyles}
            onUpdateLayout={this.setRightBranchLayout}
          >
            <Body ast={yes} />
          </YogaNode>
        </YogaNode>
      </React.Fragment>
    );
  }
}

Fork.contextTypes = {
  root: PropTypes.object
};

Fork.propTypes = {
  ast: PropTypes.object.isRequired
};

export default Fork;

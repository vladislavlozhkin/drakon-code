import React, { Component } from "react";
import PropTypes from "prop-types";
import Yoga from "yoga-layout";

import YogaNode from "../YogaNode";
import Choice from "../icons/Choice";
import Case from "../icons/Case";
import Body from "./Body";

class Switch extends Component {
  state = {
    casesLayouts: [],
    casesLayout: {},
    topLinePoints: [],
    bottomLinePoints: [],
    casesLines: []
  };

  componentDidUpdate() {
    this.setLinesPoints();
  }

  setLinesPoints() {
    this.setTopLinePoints();
    this.setBottomLinePoints();
    this.setCasesLines();
  }

  setTopLinePoints() {
    const { casesLayout, casesLayouts } = this.state;
    const lastCaseLayout = casesLayouts[casesLayouts.length - 1];
    const right = lastCaseLayout.left;
    const top = casesLayout.top + 1;
    const topLinePoints = [0, top, right, top];

    const current = this.state.topLinePoints.toString();
    const next = topLinePoints.toString();
    if (current !== next) {
      this.setState({ topLinePoints });
    }
  }

  setBottomLinePoints() {
    const { casesLayout, casesLayouts } = this.state;
    const lastCaseLayout = casesLayouts[casesLayouts.length - 1];
    const right = lastCaseLayout.left;
    const bottom = casesLayout.height + casesLayout.top - 1;
    const bottomLinePoints = [0, bottom, right, bottom];

    const current = this.state.bottomLinePoints.toString();
    const next = bottomLinePoints.toString();
    if (current !== next) {
      this.setState({ bottomLinePoints });
    }
  }

  setCasesLines() {
    const { casesLayout, casesLayouts, casesLines } = this.state;
    const lines = casesLayouts.map((layout, i) => {
      let line = null;

      if (i !== 0) {
        const left = layout.left;
        const top = casesLayout.top + 1;
        const bottom = casesLayout.height + casesLayout.top - 1;
        line = [left, top, left, bottom];
      }

      return line;
    });

    const next = lines.toString();
    const current = casesLines.toString();
    if (next !== current) {
      this.setState({ casesLines: lines });
    }
  }

  pushCaseLayout = (layout, i) => {
    const { cases } = this.props.ast.data;
    const { casesLayouts } = this.state;
    const casesCount = cases.length;

    if (i < casesCount && casesLayouts.length < casesCount) {
      casesLayouts[casesLayouts.length] = layout;
      this.setState({ casesLayouts });
    }
  };

  setCasesLayout = layout => {
    const current = this.state.casesLayout.toString();
    const next = layout.toString();
    if (current !== next) {
      this.setState({ casesLayout: layout });
    }
  };

  render() {
    const { choice, cases } = this.props.ast.data;
    const { casesLines, topLinePoints, bottomLinePoints } = this.state;

    const styles = {
      choice: {
        setMargin: [Yoga.EDGE_BOTTOM, 10]
      },
      cases: {
        setFlexDirection: [Yoga.FLEX_DIRECTION_ROW]
      },
      case: {
        setMargin: [Yoga.EDGE_BOTTOM, 10]
      },
      case_branch: {
        setMargin: [Yoga.EDGE_RIGHT, 20]
      }
    };

    return (
      <React.Fragment>
        <polyline
          fill="none"
          stroke="#282c34"
          strokeWidth={2}
          points={topLinePoints}
        />
        <polyline
          fill="none"
          stroke="#282c34"
          strokeWidth={2}
          points={bottomLinePoints}
        />
        {casesLines.map((linePoints, i) => (
          <polyline
            fill="none"
            stroke="#282c34"
            strokeWidth={2}
            points={linePoints}
            key={linePoints + i}
          />
        ))}
        <YogaNode styles={styles.choice}>
          <Choice code={choice.code} />
        </YogaNode>
        <YogaNode styles={styles.cases} onUpdateLayout={this.setCasesLayout}>
          {cases.map((caseAst, i) => (
            <YogaNode
              styles={styles.case_branch}
              key={caseAst.code + i}
              onUpdateLayout={layout => this.pushCaseLayout(layout, i)}
            >
              <YogaNode styles={styles.case}>
                <Case code={caseAst.code} />
              </YogaNode>
              <Body ast={caseAst.body} />
            </YogaNode>
          ))}
        </YogaNode>
      </React.Fragment>
    );
  }
}

Switch.propTypes = {
  ast: PropTypes.object.isRequired
};

export default Switch;

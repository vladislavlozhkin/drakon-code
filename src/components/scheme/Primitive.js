import React, { Component } from "react";
import PropTypes from "prop-types";
import Yoga from "yoga-layout";

import YogaRoot from "../YogaRoot";
import YogaNode from "../YogaNode";

import Title from "../icons/Title";
import Params from "../icons/Params";
import End from "../icons/End";
import Body from "../scheme/Body";

class Primitive extends Component {
  state = {
    titleLayout: {},
    paramsLayout: {},
    endLayout: {},
    rootLinePoints: [],
    titleWithParamsLinePoints: []
  };

  componentDidUpdate() {
    this.setLinesPoints();
  }

  setLinesPoints() {
    this.setTitleWithParamsLinePoints();
    this.setRootLinePoints();
  }

  setTitleWithParamsLinePoints() {
    const { titleLayout, paramsLayout } = this.state;
    const titleWithParamsLinePoints = [
      titleLayout.width,
      titleLayout.height / 2,
      paramsLayout.left,
      titleLayout.height / 2
    ];

    const current = this.state.titleWithParamsLinePoints.toString();
    const next = titleWithParamsLinePoints.toString();
    if (current !== next) {
      this.setState({ titleWithParamsLinePoints });
    }
  }

  setRootLinePoints() {
    const { titleLayout, endLayout } = this.state;
    const rootLinePoints = [1, titleLayout.height, 1, endLayout.top];

    const current = this.state.rootLinePoints.toString();
    const next = rootLinePoints.toString();
    if (current !== next) {
      this.setState({ rootLinePoints });
    }
  }

  setTitleLayout = layout => {
    const current = this.state.titleLayout.toString();
    const next = layout.toString();
    if (current !== next) {
      this.setState({ titleLayout: layout });
    }
  };

  setParamsLayout = layout => {
    const current = this.state.paramsLayout.toString();
    const next = layout.toString();
    if (current !== next) {
      this.setState({ paramsLayout: layout });
    }
  };

  setEndLayout = layout => {
    const current = this.state.endLayout.toString();
    const next = layout.toString();
    if (current !== next) {
      this.setState({ endLayout: layout });
    }
  };

  render() {
    const { ast } = this.props;
    const { rootLinePoints, titleWithParamsLinePoints } = this.state;

    const titleWithParams = {
      setFlexDirection: [Yoga.FLEX_DIRECTION_ROW],
      setMargin: [Yoga.EDGE_BOTTOM, 10]
    };

    const titleWrapper = {
      setMargin: [Yoga.EDGE_RIGHT, 20]
    };

    return (
      <g className="Primitive">
        {ast && (
          <YogaRoot>
            <YogaNode styles={titleWithParams}>
              <YogaNode
                styles={titleWrapper}
                onUpdateLayout={this.setTitleLayout}
              >
                <Title code={ast.title.code} />
              </YogaNode>
              <YogaNode onUpdateLayout={this.setParamsLayout}>
                <Params params={ast.params} />
              </YogaNode>
            </YogaNode>
            {ast.body && <Body ast={ast.body} />}
            <YogaNode onUpdateLayout={this.setEndLayout}>
              <End code={"End"} />
            </YogaNode>
          </YogaRoot>
        )}
        <polyline
          fill="none"
          stroke="#282c34"
          strokeWidth={2}
          points={rootLinePoints}
        />
        <polyline
          fill="none"
          stroke="#282c34"
          strokeWidth={2}
          points={titleWithParamsLinePoints}
        />
      </g>
    );
  }
}

Primitive.propTypes = {
  ast: PropTypes.object.isRequired
};

export default Primitive;

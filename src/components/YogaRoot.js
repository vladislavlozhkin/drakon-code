import { Component } from "react";
import PropTypes from "prop-types";
import Yoga from "yoga-layout";

class YogaRoot extends Component {
  getChildContext() {
    const node = Yoga.Node.create();
    return {
      root: node,
      parent: node
    };
  }

  render() {
    return this.props.children;
  }
}

YogaRoot.childContextTypes = {
  root: PropTypes.object,
  parent: PropTypes.object
};

export default YogaRoot;
